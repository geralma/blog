class AddArticuloIdToComentario < ActiveRecord::Migration
  def change
    add_column :comentarios, :articulo_id, :integer
    add_index :comentarios, :articulo_id
  end
end
